(async () => {
    home.select('meta[property="og:url"]').content += `/${username}`;
    if (username !== '') {
        const response_preuser = await fetch(`https://gitlab.com/api/v4/users/?username=${username}`);
        const json_preuser = await response_preuser.json();
        if (json_preuser == '') {
            home.select('#loading').classList.add('d-none');
            home.select('#notfound').classList.remove('d-none');
            home.select('.navbar').classList.add('d-flex');
            home.select('.navbar').classList.remove('d-none');
            return false;
        }
        let isUser;
        json_preuser.forEach(user => {
            isUser = user.id;
        });
        const response_user = await fetch(`https://gitlab.com/api/v4/users/${isUser}`);
        const response_rep = await fetch(`https://gitlab.com/api/v4/users/${isUser}/projects?per_page=100`);
        const response_event = await fetch(`https://gitlab.com/api/v4/users/${isUser}/events?order_by=created_at`);
        const json_user = await response_user.json();
        const json_rep = await response_rep.json();
        const json_event = await response_event.json();

        const gitSearchElement = home.select('#search_first');
        const gitAvatarElement = home.select('#avatar');
        const gitAvatarTwitter = home.select('meta[name="twitter:image"]');
        const gitAvatarOG = home.select('meta[property="og:image"]');
        const gitNameElement = home.select('#name');
        const gitTitle = home.select('title');
        const gitTitleTwitter = home.select('meta[name="twitter:title"]');
        const gitTitleOG = home.select('meta[property="og:title"]');
        const gitBlogElement = home.select('#blog');
        const gitCompanyElement = home.select('#company');
        const gitMoreElement = home.select('#more');
        const gitDescription = home.select('meta[name="description"]');
        const gitDescriptionTwitter = home.select('meta[name="twitter:description"]');
        const gitDescriptionOG = home.select('meta[property="og:description"]');
        const gitActivitiesElement = home.select('#activities');

        let allStars = 0;
        let allForks = 0;
        json_rep.forEach(repo => {
            allStars += repo.star_count;
            allForks += repo.forks_count;
        });

        // username in search bar
        gitSearchElement.value = json_user.username;

        // avatar
        gitAvatarElement.src = json_user.avatar_url;
        gitAvatarTwitter.content = json_user.avatar_url;
        gitAvatarOG.content = json_user.avatar_url;

        gitNameElement.href = json_user.web_url; // url to name
        if (json_user.name === '') { // login if name 'null'
            gitNameElement.textContent = json_user.username;
            gitTitle.textContent = `${json_user.username} | GitLab Stat`;
            gitTitleTwitter.content = json_user.username;
            gitTitleOG.content = json_user.username;
        } else { // name
            gitNameElement.textContent = json_user.name;
            gitTitle.textContent = `${json_user.name} | GitLab Stat`;
            gitTitleTwitter.content = json_user.name;
            gitTitleOG.content = json_user.name;
        }

        // website url
        if (json_user.website_url !== '') {
            gitNameElement.innerHTML += '<br>';
            gitBlogElement.href = json_user.website_url; // todo: fix for non http url
            gitBlogElement.textContent = `${json_user.website_url.replace(/^(http:|https:)?\/*/i,"")}`;
        }

        // organization
        if (json_user.organization !== '') {
            if (json_user.website_url !== '') 
                gitBlogElement.innerHTML += '<br>';
            else 
                gitNameElement.innerHTML += '<br>';
            gitCompanyElement.className = `badge badge-primary`;
            gitCompanyElement.innerHTML = `<i class="fas fa-users fa-fw mr-2"></i>${json_user.organization}`;
        }

        // bio
        if (json_user.bio !== '') {
            gitMoreElement.innerHTML += `<li class="list-group-item"><small class="text-muted">Bio:</small><p class="m-0">${json_user.bio}</p></li>`;
            gitDescription.content = json_user.bio;
            gitDescriptionTwitter.content = json_user.bio;
            gitDescriptionOG.content = json_user.bio;
        }

        // counts
        gitMoreElement.innerHTML += `
        <li class="list-group-item d-flex flex-column">
            <div><div class="float-left text-muted">Stars Received</div><div class="text-monospace float-right">${allStars}</div></div>
            <div><div class="float-left text-muted">Forks by users</div><div class="text-monospace float-right">${allForks}</div></div>
        </li>`;

        // about (join date, location)
        let gitAboutElement = `<small class="text-muted">Joined</small><p>${isDate(json_user.created_at)}</p>`;
        if (json_user.location !== null)
            gitAboutElement += `<small class="text-muted">Location</small><p class="text-primary">${json_user.location}</p>`;
        gitMoreElement.innerHTML += `<li class="list-group-item">${gitAboutElement}</li>`;

        // activities
        let plusIcon, branchIcon, tagIcon, trashIcon, banIcon, commentIcon;
        plusIcon = `<i class="fas fa-plus-circle fa-fw text-success"></i>`;
        branchIcon = `<i class="fas fa-code-branch fa-fw text-success"></i>`;
        tagIcon = `<i class="fas fa-tag fa-fw text-success"></i>`;
        trashIcon = `<i class="fas fa-trash-alt fa-fw text-danger"></i>`;
        banIcon = `<i class="fas fa-ban fa-fw text-danger"></i>`;
        commentIcon = `<i class="fas fa-comment-dots fa-fw text-muted"></i>`;
        let eventid = [];
        for (let activities of json_event) {
            const response_event_rep = await fetch(`https://gitlab.com/api/v4/projects/${activities.project_id}`);
            const json_event_rep = await response_event_rep.json();
            repoURL = `<a href="${json_event_rep.web_url}">${json_event_rep.path_with_namespace}</a>`;
            switch (activities.action_name) {
                case "pushed to":
                    let commit = `commit`;
                    if (activities.push_data.commit_count > 1)
                        commit += "s";
                    isStr = `${plusIcon} Pushed <a href="https://gitlab.com/${json_event_rep.path_with_namespace}/commits/${activities.push_data.ref}">${activities.push_data.commit_count} ${commit}</a> to ${repoURL}`;
                    break;
                case "created":
                    isStr = `${plusIcon} Created a repository ${repoURL}`;
                    break;
                case "pushed new":
                    if (activities.push_data.ref_type === "branch")
                        isStr = branchIcon;
                    if (activities.push_data.ref_type === "tag")
                        isStr = tagIcon;
                    else
                        isStr = plusIcon;
                    isStr += ` Created a ${activities.push_data.ref_type} `;
                    if (activities.push_data.ref_type === "branch")
                        isStr += `<a href="${json_event_rep.web_url}/tree/${activities.push_data.ref}">${activities.push_data.ref}</a> in ${repoURL}`;
                    else
                        isStr += repoURL;
                    break;
                case "opened":
                    if (activities.target_type === 'MergeRequest') {
                        isStr = branchIcon;
                        url = 'merge_request';
                        name = 'Merge Request';
                    } else {
                        isStr = plusIcon;
                        url = activities.target_type.toLowerCase();
                        name = activities.target_type.toLowerCase();
                    }
                    isStr += ` Opened an <a href="${json_event_rep.web_url}/${url}/${activities.target_iid}">${name}</a> in ${repoURL}`;
                    break;
                case "closed":
                    if (activities.target_type === 'MergeRequest') {
                        url = 'merge_request';
                        name = 'Merge Request';
                    } else {
                        url = activities.target_type.toLowerCase();
                        name = activities.target_type.toLowerCase();
                    }
                    isStr = `${banIcon} Closed an <a href="${json_event_rep.web_url}/${url}/${activities.target_iid}">${name}</a> in ${repoURL}`;
                    break;
                case "accepted":
                    if (activities.target_type === 'MergeRequest') {
                        url = 'merge_request';
                        name = 'Merge Request';
                    } else {
                        url = activities.target_type.toLowerCase();
                        name = activities.target_type.toLowerCase();
                    }
                    isStr = `${plusIcon} Accepted an <a href="${json_event_rep.web_url}/${url}/${activities.target_iid}">${name}</a> in ${repoURL}`;
                    break;
                case "commented on":
                    isStr = `${commentIcon} Created <a href="${json_event_rep.web_url}/${activities.note.noteable_type.toLowerCase()}/${activities.note.noteable_iid}#note_${activities.note.id}">a comment</a> on an issue in ${repoURL}`;
                    break;
                case "deleted":
                    isStr = `${trashIcon} Deleted a ${activities.push_data.ref_type} ${activities.push_data.ref} from ${repoURL}`;
                    break;
                default:
                    console.log(activities.action_name);
                    return false;
                    break;
            }
            gitActivitiesElement.innerHTML += `<li class="list-group-item"><div class="row"><div class="col-9">${isStr}</div><div class="col-3 text-muted text-right">${isTimeAgo(activities.created_at)}</div></div></li>`;
        }
        home.select('#loading').classList.add('d-none');
        home.select('#profile').classList.remove('d-none');
        home.select('.navbar').classList.add('d-flex');
        home.select('.navbar').classList.remove('d-none');
    }
})();